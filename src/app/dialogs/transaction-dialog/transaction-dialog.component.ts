import {Component, inject} from '@angular/core';
import {CurrencyPipe} from "@angular/common";
import {FormControl, FormGroup, FormsModule, ReactiveFormsModule, Validators} from "@angular/forms";
import {MatButton} from "@angular/material/button";
import {MatDatepicker, MatDatepickerInput, MatDatepickerToggle} from "@angular/material/datepicker";
import {
  MAT_DIALOG_DATA,
  MatDialogActions,
  MatDialogClose,
  MatDialogContent,
  MatDialogRef,
  MatDialogTitle
} from "@angular/material/dialog";
import {MatFormField, MatHint, MatLabel, MatSuffix} from "@angular/material/form-field";
import {MatInput} from "@angular/material/input";
import {DBService} from "../../services/db.service";
import {MatOption, MatSelect} from "@angular/material/select";
import {TRANSACTION_TYPES} from "../../app.constants";
import {DateTime} from "luxon";

@Component({
  selector: 'app-transaction-dialog',
  standalone: true,
  imports: [
    CurrencyPipe,
    FormsModule,
    MatButton,
    MatDatepicker,
    MatDatepickerInput,
    MatDatepickerToggle,
    MatDialogActions,
    MatDialogClose,
    MatDialogContent,
    MatDialogTitle,
    MatFormField,
    MatHint,
    MatInput,
    MatLabel,
    MatSuffix,
    ReactiveFormsModule,
    MatSelect,
    MatOption
  ],
  templateUrl: './transaction-dialog.component.html',
  styleUrl: './transaction-dialog.component.scss'
})
export class TransactionDialogComponent {
  protected data = inject(MAT_DIALOG_DATA);
  protected transaction = this.data.transaction;
  formGroup = new FormGroup({
    source: new FormControl(this.transaction.source, [Validators.required]),
    description: new FormControl(this.transaction.description, []),
    type: new FormControl(this.transaction.type, [Validators.required]),
    date: new FormControl(this.transaction.date, [Validators.required]),
    amount: new FormControl(this.transaction.amount, [Validators.required]),
  });
  protected dbService = inject(DBService);
  protected modalRef = inject(MatDialogRef);
  protected readonly TRANSACTION_TYPES = TRANSACTION_TYPES;

  async onSubmit() {
    if (this.formGroup.invalid) return;
    const data = {...this.transaction, ...this.formGroup.value};
    data.dateAsNumber = Number(DateTime.fromISO(data.date).toFormat('yyyyMMdd'));

    const result = await this.dbService.upsertTransaction(data);
    if (result) this.modalRef.close();
    console.log(this.formGroup.value);
  }
}
