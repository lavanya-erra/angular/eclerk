import {Component, Input} from '@angular/core';
import {AgChartsAngular} from "ag-charts-angular";
import {CurrencyPipe} from "@angular/common";

@Component({
  selector: 'app-donut-chart',
  standalone: true,
  imports: [AgChartsAngular, CurrencyPipe],
  templateUrl: './donut-chart.component.html',
  styleUrl: './donut-chart.component.scss'
})
export class DonutChartComponent {
  @Input() data: any;
}
