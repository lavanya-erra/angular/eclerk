import {Component, inject, Input, OnInit} from '@angular/core';
import {balance$, deleteTransaction$, showToast$, transactionSummary$} from "../../store";
import {CurrencyPipe} from "@angular/common";
import {MatButton, MatIconButton} from "@angular/material/button";
import {
  MatCard,
  MatCardActions,
  MatCardContent,
  MatCardHeader,
  MatCardSubtitle,
  MatCardTitle
} from "@angular/material/card";
import {MatDivider} from "@angular/material/divider";
import {MatIcon} from "@angular/material/icon";
import {MatMenu, MatMenuContent, MatMenuItem, MatMenuTrigger} from "@angular/material/menu";
import {MatDialog} from "@angular/material/dialog";
import {Transaction} from "../../models/transaction";
import {TransactionDialogComponent} from "../../dialogs/transaction-dialog/transaction-dialog.component";

@Component({
  selector: 'app-transaction-card',
  standalone: true,
  imports: [
    CurrencyPipe,
    MatButton,
    MatCard,
    MatCardActions,
    MatCardContent,
    MatCardHeader,
    MatCardSubtitle,
    MatCardTitle,
    MatDivider,
    MatIcon,
    MatIconButton,
    MatMenu,
    MatMenuContent,
    MatMenuItem,
    MatMenuTrigger
  ],
  templateUrl: './transaction-card.component.html',
  styleUrl: './transaction-card.component.scss'
})
export class TransactionCardComponent implements OnInit {
  @Input() transactions: Transaction[];
  protected readonly balance$ = balance$;
  protected readonly transactionSummary$ = transactionSummary$;
  protected readonly deleteTransaction$ = deleteTransaction$;
  protected readonly showToast$ = showToast$;
  private matDialog = inject(MatDialog);

  ngOnInit() {
  }

  openTransactionDialog(transaction = new Transaction()) {
    const data = {transaction, isNew: true}
    this.matDialog.open(TransactionDialogComponent, {data});
  }
}
