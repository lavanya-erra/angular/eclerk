import {Component} from '@angular/core';
import {
  MatCard,
  MatCardActions,
  MatCardContent,
  MatCardHeader,
  MatCardSubtitle,
  MatCardTitle
} from "@angular/material/card";
import {MatIcon} from "@angular/material/icon";
import {MatButton, MatIconButton} from "@angular/material/button";
import {transactions$, transactionSummary$} from "../../store";
import {AsyncPipe, CurrencyPipe, JsonPipe} from "@angular/common";
import {MatDivider} from "@angular/material/divider";
import {MatList, MatListItem, MatListItemLine, MatListItemTitle} from "@angular/material/list";
import {MatMenu, MatMenuContent, MatMenuItem, MatMenuTrigger} from "@angular/material/menu";
import {MatChip} from "@angular/material/chips";
import {DonutChartComponent} from "../../components/donut-chart/donut-chart.component";
import {TransactionCardComponent} from "../../components/transaction-card/transaction-card.component";

@Component({
  selector: 'app-dashboard',
  standalone: true,
  imports: [
    MatCard,
    MatCardHeader,
    MatCardSubtitle,
    MatCardTitle,
    MatIcon,
    MatIconButton,
    MatButton,
    MatCardContent,
    JsonPipe,
    AsyncPipe,
    CurrencyPipe,
    MatDivider,
    MatList,
    MatListItem,
    MatListItemLine,
    MatListItemTitle,
    MatMenuTrigger,
    MatCardActions,
    MatMenu,
    MatMenuContent,
    MatMenuItem,
    MatChip,
    DonutChartComponent,
    TransactionCardComponent
  ],
  templateUrl: './dashboard.component.html',
  styleUrl: './dashboard.component.scss'
})
export class DashboardComponent {
  protected readonly transactions$ = transactions$;
  protected readonly transactionSummary$ = transactionSummary$;
}
