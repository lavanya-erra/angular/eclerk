import {computed, Signal, signal, WritableSignal} from "@angular/core";
import {buildDonutChart} from "../../app.util";

const calculateBalance = (a, b) => {
  if (b.type === 'credit') return a + b.amount;
  return a - b.amount;
}

const groupTransactions = (a, b) => {
  if (b.type === 'credit') {
    a.income.push(b);
  } else {
    a.expense.push(b);
  }
  return a;
}

export const transactionDatabase$ = signal(undefined);
export const transactions$: WritableSignal<any> = signal(undefined);
export const transactionSummary$: Signal<any> = computed(() => {
  const transactions = transactions$();
  console.log('transactionSummary$', transactions);
  if (transactions) {
    const summary = transactions?.reduce(groupTransactions, {income: [], expense: []})
    return buildDonutChart(summary);
  }
  return undefined;
});
export const balance$ = computed(() => transactions$()?.reduce(calculateBalance, 0));
export const deleteTransaction$: WritableSignal<any> = signal(undefined, {equal: () => false});
