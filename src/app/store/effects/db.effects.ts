import {effect, inject, Injectable} from "@angular/core";
import {deleteTransaction$, transactionDatabase$} from "../signals";
import {DBService} from "../../services/db.service";

@Injectable({providedIn: 'root'})
export class DBEffects {
  private dbService: DBService = inject(DBService);

  deleteTransactionEffect = effect(async () => {
    const transaction = deleteTransaction$();
    if (transaction) {
      await this.dbService.deleteTransaction(transaction);
    }
  }, {allowSignalWrites: true});

  loadTransactionsEffect = effect(async () => {
    if (transactionDatabase$()) {
      await this.dbService.loadTransactions();
    }
  }, {allowSignalWrites: true});
}
