import {AppEffects} from "./app.effects";
import {DBEffects} from "./db.effects";

export const EFFECTS = [AppEffects, DBEffects];
