import {ResolveFn} from '@angular/router';
import {Auth} from "@angular/fire/auth";
import {inject} from "@angular/core";
import {transactionDatabase$, user_uid$} from "../store";
import {Database, ref} from "@angular/fire/database";

export const authResolver: ResolveFn<boolean> = (route, state) => {
  const auth = inject(Auth);
  const uuid = auth.currentUser?.uid;
  user_uid$.set(uuid);

  const database: Database = inject(Database);
  const dbRef = ref(database, `transactions/${user_uid$()}`);
  transactionDatabase$.set(dbRef);
  return true;
};
