export const buildDonutChart = (summary) => {
  const totalIncome = summary.income.reduce((a, b) => a + b.amount, 0);
  const totalExpense = summary.expense.reduce((a, b) => a + b.amount, 0);
  const data = [
    {asset: 'Income', amount: totalIncome},
    {asset: 'Expense', amount: totalExpense}
  ];
  const balance = (totalIncome - totalExpense).toLocaleString('en-US', {style: 'currency', currency: 'USD'});
  const options = {
    data,
    title: {
      text: `Total Balance: ${balance}`,
    },
    series: [
      {
        type: "donut",
        calloutLabelKey: "asset",
        angleKey: "amount",
        innerRadiusRatio: 0.5,
      },
    ],
  };
  console.log(options, 'options');
  return options;
}
