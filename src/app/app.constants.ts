export const TRANSACTION_TYPES = [
  {name: 'Credit', value: 'credit', description: 'Money received'},
  {name: 'Debit', value: 'debit', description: 'Money spent'}
]
