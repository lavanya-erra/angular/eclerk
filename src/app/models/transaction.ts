export class Transaction {
  id?: string = crypto.randomUUID();
  amount?: number;
  source?: string;
  date?: string = new Date().toISOString();
  description?: string;
  type?: string;
}
