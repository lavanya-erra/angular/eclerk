import {HttpInterceptorFn} from '@angular/common/http';
import {showLoader$} from "../store";
import {finalize} from "rxjs";

export const httpInterceptor: HttpInterceptorFn = (req, next) => {
  showLoader$.set(true);
  return next(req).pipe(finalize(() => showLoader$.set(false)));
};
