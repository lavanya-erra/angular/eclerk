import {inject, Injectable} from '@angular/core';
import {child, Database, onValue, remove, set} from '@angular/fire/database';
import {showToast$, transactionDatabase$, transactions$} from "../store";
import {Transaction} from "../models/transaction";

@Injectable({
  providedIn: 'root'
})
export class DBService {
  private database: Database = inject(Database);

  async loadTransactions() {
    try {
      onValue(transactionDatabase$(), (snapshot) => {
        const data = snapshot.val();
        console.log(data, 'from db service');
        const transactions = data ? Object.values(data) : [];
        transactions$.set(transactions);
      });
    } catch (error) {
      showToast$.set(error.message);
    }
  }

  /**
   * Upsert's a transaction into the database.
   *
   * @param transaction - the transaction to upsert
   * @returns true if the transaction was upserted, false otherwise
   */
  async upsertTransaction(transaction: Transaction): Promise<boolean> {
    console.log(transaction, 'from db service');
    const dbRef = child(transactionDatabase$(), transaction.id);
    try {
      await set(dbRef, transaction);
      showToast$.set('Transaction saved successfully');
      return true;
    } catch (error) {
      showToast$.set(error.message);
    }
    return false;
  }

  /**
   * Deletes transaction from the database.
   *
   * @param transaction - the income to delete
   * @returns true if the transaction was deleted, false otherwise
   */
  async deleteTransaction(transaction: Transaction): Promise<boolean> {
    console.log('Deleting', transaction.id, 'from db service');
    const dbRef = child(transactionDatabase$(), transaction.id);
    try {
      await remove(dbRef);
      showToast$.set('Transaction deleted successfully');
      return true;
    } catch (error) {
      showToast$.set(error.message);
    }
    return false;
  }
}
